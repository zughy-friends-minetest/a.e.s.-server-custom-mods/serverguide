# Server Guide

Official in-game guides for A.E.S. server, containing:
* `/rules` command
* "first steps" formspec + `/tutorial` command
* "our values" formspec
* pedestals
