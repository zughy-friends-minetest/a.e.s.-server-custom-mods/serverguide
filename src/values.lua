local S = core.get_translator("serverguide")
local FS = function(...) return core.formspec_escape(S(...)) end

local function get_formspec() end



function serverguide.get_values_formspec(p_name)
  core.show_formspec(p_name, "serverguide:values", get_formspec())
end





----------------------------------------------
---------------FUNZIONI LOCALI----------------
----------------------------------------------

function get_formspec()
  local formspec = {
  "formspec_version[4]",
  "size[12,17.3]",
  "position[0.5,0.47]",
  "no_prepend[]",
  "bgcolor[;true]",
  "background[0,0;12,17.3;aesguide_values.png]",
  "hypertext[1,0.9;10,2;weap_txt;<global size=26 halign=center valign=middle font=mono><style color=#302c2e><b>" .. FS("OUR VALUES") .. "</b>]",
  "hypertext[3.5,2.98;3.9,1.2;pname_txt;<global size=13 font=mono halign=center valign=middle color=#302c2e><b>" .. FS("EMPATHY") .. "</b>]",
  "hypertext[3.5,3.98;7.1,2;pname_txt;<global size=12 font=mono valign=middle color=#302c2e>" .. FS("Games shouldn't hurt people: we refuse any sort of suffering that might generate from making games - whether is players' or devs'. Things like gambling, manipulation and exploitation don't belong here.") .. "</b>]",
  "hypertext[4.15,6.86;3.9,1.2;pname_txt;<global size=13 font=mono halign=center valign=middle color=#302c2e><b>" .. FS("SKILL-BASED") .. "</b>]",
  "hypertext[1.35,7.9;7.1,1.2;pname_txt;<global size=12 font=mono valign=middle color=#302c2e>" .. FS("People's wallets shouldn't make them stronger: say no to pay to win!") .. "]",
  "hypertext[3.64,10.06;3.9,1.2;pname_txt;<global size=13 font=mono halign=center valign=middle color=#302c2e><b>" .. FS("PRIVACY") .. "</b>]",
  "hypertext[3.5,11.12;7.1,1.2;pname_txt;<global size=12 font=mono valign=middle color=#302c2e>" .. FS("We don't collect data about you (but we do collect data about minigames, @1have a look@2!)", "<action name=grafana_url url=https://monitoring.aes.land/>", "</action><b></b>") .. "]", -- <b> dopo </action> per via di https://github.com/minetest/minetest/issues/15500
  "hypertext[4.15,13.3;3.9,1.2;pname_txt;<global size=13 font=mono halign=center valign=middle color=#302c2e><b>" .. FS("FREE SOFTWARE") .. "</b>]",
  "hypertext[1.35,14.36;7.1,1.2;pname_txt;<global size=12 font=mono valign=middle color=#302c2e>" .. FS("Our code and assets belong to everyone: @1check it out@2!", "<action name=repo_url url=https://gitlab.com/zughy-friends-minetest>", "</action><b></b>") .. "]", -- idem come sopra
  "image_button_exit[12.3,0.39;0.65,0.65;arenalib_infobox_quit.png;quit;;true;false;]"
  }

  return table.concat(formspec, "")
end





----------------------------------------------
---------------GESTIONE CAMPI-----------------
----------------------------------------------


core.register_on_player_receive_fields(function(player, formname, fields)
	if formname ~= "serverguide:steps" then return end

	if fields.quit and not fields.confirm then
    core.show_formspec(player:get_player_name(), "serverguide:steps", get_formspec())
    return
  end
end)