local S = core.get_translator("serverguide")



-- nodi
core.register_node("serverguide:pedestal_rules", {
	drawtype = "mesh",
	mesh = "fbrawl_book_pedestal.obj",
	tiles = {"aesguide_pedestal.png"},
	visual_scale = 1.4,
	selection_box = {
		type = "fixed",
		fixed  = {-0.3, -0.5, -0.3, 0.3, 0.12, 0.3}
	},
	use_texture_alpha = "clip",
	groups = {oddly_breakable_by_hand=1},
	paramtype = "light",
	paramtype2 = "facedir",
	description = S("Rules"),

  on_construct = function(pos)
    core.add_entity(vector.add(pos, vector.new(0,0,0)), "serverguide:rules")
  end,

	on_punch = function (pos, node, puncher)
		aes_tos.show_rules(puncher:get_player_name())
	end,

	on_rightclick = function (pos, node, clicker)
		aes_tos.show_rules(clicker:get_player_name())
	end
})



core.register_node("serverguide:pedestal_values", {
	drawtype = "mesh",
	mesh = "fbrawl_book_pedestal.obj",
	tiles = {"aesguide_pedestal.png"},
	visual_scale = 1.4,
	selection_box = {
		type = "fixed",
		fixed  = {-0.3, -0.5, -0.3, 0.3, 0.12, 0.3}
	},
	use_texture_alpha = "clip",
	groups = {oddly_breakable_by_hand=1},
	paramtype = "light",
	paramtype2 = "facedir",
	description = S("About"),

  on_construct = function(pos)
    core.add_entity(vector.add(pos, vector.new(0,0,0)), "serverguide:values")
  end,

	on_punch = function (pos, node, puncher)
		serverguide.get_values_formspec(puncher:get_player_name())
	end,

	on_rightclick = function (pos, node, clicker)
		serverguide.get_values_formspec(clicker:get_player_name())
	end
})




-- entità
local rules_txt = {
  initial_properties = {
    visual = "upright_sprite",
    textures = {"blank.png"},
    physical = false,
	pointable = false,
    static_save = true,
    nametag = S("Rules"),
  },
}


local values_txt = {
	initial_properties = {
	  visual = "upright_sprite",
	  textures = {"blank.png"},
	  physical = false,
	  pointable = false,
	  static_save = true,
	  nametag = S("About"),
	},
  }



core.register_entity("serverguide:rules", rules_txt)
core.register_entity("serverguide:values", values_txt)