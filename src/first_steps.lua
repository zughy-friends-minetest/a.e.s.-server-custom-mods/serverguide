local S = minetest.get_translator("serverguide")

local function get_formspec() end



function serverguide.get_firststeps_formspec(p_name)
  minetest.show_formspec(p_name, "serverguide:steps", get_formspec())
end




----------------------------------------------
---------------FUNZIONI LOCALI----------------
----------------------------------------------

function get_formspec()
  local formspec = {
  "formspec_version[4]",
  "size[14,18]",
  "position[0.5,0.47]",
  "no_prepend[]",
  "bgcolor[;neither]",
  "style_type[button;border=false]",
  "background[0,0;1,1;aesguide_first_steps.png;true]",
  "hypertext[4.3,4.2;8,3;name;<global valign=middle><style size=16 font=mono color=#ffffff>1. " .. S("Use your free hand to hit an arena sign and enter a minigame (/quit to leave)") .. "</style>]",
  "hypertext[1.75,8.5;9.2,3;name;<global valign=middle><style size=16 font=mono color=#ffffff>2. " .. S("Use the compass to teleport and reach places more quickly") .. "</style>]",
  "hypertext[4.3,12.7;8,3;name;<global valign=middle><style size=16 font=mono color=#ffffff>3. " .. S("Complete many challenges and unlock content to make your adventure more magical!") .. "</style>]",
  "hypertext[1.3,16.33;2.5,0.5;name;<global valign=middle><style size=16 font=mono color=#eea160>/tutorial</style>]",
  "button_exit[9.82,16.1;3.8,0.97;confirm;" .. S("LET'S GO!") .. "]"
  }

  return table.concat(formspec, "")
end





----------------------------------------------
---------------GESTIONE CAMPI-----------------
----------------------------------------------


minetest.register_on_player_receive_fields(function(player, formname, fields)
	if formname ~= "serverguide:steps" then return end

	if fields.quit and not fields.confirm then
    minetest.show_formspec(player:get_player_name(), "serverguide:steps", get_formspec())
    return
  end
end)